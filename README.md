# Framer

## Install

```
pip install git+ssh://git@gitlab.com/jabaliesrc/videotools/framer.git@main
```

## Use

```
framer --video /mnt/jrc/crudo/JRC-SANMA-11-09.mp4 --fps=1 --frame=1 --frames=10 --output='.'
```

will extract 10 frames at 1 fps starting at frame at `00:00:01`
