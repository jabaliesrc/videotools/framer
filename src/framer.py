import ffmpeg
import click
import os
from pathlib import Path

@click.command()
@click.option('--video', required=True, help='Input video')
@click.option('--frame', default=0, help='Output video')
@click.option('--frames', default=1, help='Number of frames to process')
@click.option('--fps', default=1, help='Frames per second')
@click.option('--output', default='.', help='Output path')
def framer(video, frame, frames, fps, output):
    png_prefixes = Path(video).stem
    out, err = (
        ffmpeg.input(video, ss=frame)
                .output(f'{output}/{png_prefixes}_%05d.png',  vf=f"fps={fps}", frames=frames)
                .run()
    )

if __name__ == '__main__':
    framer()